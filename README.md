# Changelog Generator

Little changelog generator written in python. It uses git log output to generate the changelog, so you needs to strictfully respect a commiting convention to make it works. 

## Installation

The installation of this tool is pretty simple. But, in order to use it, you need python3 installed on your system.

Firstly, clone the repo in whatever folder. Then go in the cloned-directory and launch the setup script:
```
cd changelog-generator
python setup.py
```
It will automatically install itself and clean the cloned repo of your system.

N.B: you need to restart your shell to be able to use the tool.

## Generate a changelog

To generate a changelog with this tool, you firstly need to go in your project root folder (it has to be a git repository to work).
Merge your branche into main and then, just run this command to generate the changelog : `chlog --generate`

You can control the versioning on the changelog by running one of these following commands before generating it:
``` 
chlog --version n //New major version
chlog --version u //New features version
chlog --version f //New fix,doc.. version
```

The tool will check the formatting of your commits according to it settings and warn you if their is any errors.
If you only want to check your commits' formatting, you can run  `chlog --check`, it'll output all the commits containing errors with the corresponding error to help you correct it.

## Customizing commits' formatting rules

To customize easily commits' formatting rules, you have to use the edit tool by running `chlog --rules e`. You'll be guided in the customization step by step.
If you only want to see the rules, just type `chlog --rules s`.

## List of commands

To have a complete overview of all available commands, just run `chlog --help`

## Contributing

If you want to contribute and make some change to this tool, you're welcome ! To do that, you first need to clone the repo
with `git clone`. Then go in the git directory and launch the `setup.sh` script. 

When the setup script has finished his job, you can work on the project !

## License
 
This project is licenced under GNU GPL, see the LICENSE file at the top of the repository for more details.
