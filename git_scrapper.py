import subprocess
import os
import re

class Commit():
    """ Class that represent a git commit """
    def __init__(self, hash: str, ctype: str, message: str, date: str):
        self.__hash: str = hash
        self.__ctype: str = ctype
        self.__message: str = message
        self.__date: str = date


    def get_hash(self) -> str:
        return self.__hash

    
    def get_type(self) -> str:
        return self.__ctype


    def chlog_format(self) -> str:
        """ Method that return the commit infos as a chlog file's formatted style """
        return self.__hash + ': ' + self.__message + '(' + self.__date + ')'


    def __str__(self) -> str:
        return self.__date + " " + self.__ctype + ": " + self.__message + "(" + self.__hash + ")"


def get_formatted_commits(version: str) -> list[Commit]:
    """ Method that retrieve the correctly formatted commit after the given version """
    # Lauching the git log command to retrieve all the commits and split it 
    cmd = subprocess.check_output(
        ['git', 'log', '--format=%h/%s/%as'], stderr=subprocess.STDOUT
    ).split(b'\n')
    lines = [c.decode('utf-8') for c in cmd] # Decoding each line from byte to string
    lines = lines[:-1] #Deleting last element, corresponding to the first commit of the repository

    # Parsing each commits info to create a Commit object
    commits: list[Commit] = []
    for line in lines:
        if not 'Merge' in line:
            elem = line.split('/') # Splitting element
            format_pattern = re.compile("[a-z]+:(\s|\S)(?:[a-z]+(\s|\S))+")
            if format_pattern.match(elem[1]):
                temp = elem[1].split(':') # Splitting commit type and commit subject
            if len(temp) == 2:
                elem[1] = temp[0]
                elem.insert(2, temp[1][1:]) 
                commits.append(Commit(elem[0], elem[1], elem[2], elem[3])) # Creating commit object
    
    # Getting the commits concerning the given version
    last_version_hash = get_tagged_commit(version)
    start = 0
    for c in commits:
        if c.get_hash() == last_version_hash:
            start = commits.index(c)

    return commits[:start]


def get_raw_commits(version: str) -> list[list[str]]:
    """ Method that retrieves all the raw commits after the given version """
     # Lauching the git log command to retrieve all the commits and split it 
    cmd = subprocess.check_output(
        ['git', 'log', '--format=%h/%s/%as'], stderr=subprocess.STDOUT
    ).split(b'\n')
    lines = [c.decode('utf-8') for c in cmd] # Decoding each line from byte to string
    lines = lines[:-2] #Deleting last element, corresponding to the first commit of the repository

    # Getting raw commits
    commits: list[str] = []
    for line in lines:
        if not 'Merge' in line:
            commits.append(line.split('/')[:2])

    # Getting the commits concerning the given version
    last_version_hash = get_tagged_commit(version)
    start = 0
    for c in commits:
        if c[0] == last_version_hash:
            start = commits.index(c)

    return commits[:start]

    

def add_git_tag(version: str, commit_hash: str):
    """ Method that add a git tag on the last commit or to a specific commit """
    # Simply executing the git tag commands. If commit_hash is None, we apply the tag to the last commit
    if commit_hash == None:
        subprocess.check_output(
            ['git', 'tag', '-a', version, '-m', 'Tag for version ' + version[1:]], stderr=subprocess.STDOUT
        )
        subprocess.check_output(
            ['git', 'push', '--tags'], stderr=subprocess.STDOUT
        )
    else:
        subprocess.check_output(
            ['git', 'tag', '-a', version, '-m', 'Tag for version ' + version, ' ' + commit_hash], stderr=subprocess.STDOUT
        )
    

def remove_git_tag(version):
    """ Method that remove a git tag from the repo """
    # Removing the specific tag from the local repo and from the distant one
    subprocess.check_output(
        ['git', 'tag', '-d', version], stderr=subprocess.STDOUT
    )
    subprocess.check_output(
        ['git', 'push', '--delete origin', version], stderr=subprocess.STDOUT
    )


def get_tagged_commit(version: str) -> str:
    """ Method that get the hash of the commit tagged with the given version """
    # First we run the git show commands by specifing the target version
    cmd = subprocess.check_output(
        ['git', 'rev-list', '-n', '1', version, '--format=%h'], stderr=subprocess.STDOUT
    ).split(b'\n')
    tag = [c.decode('utf-8') for c in cmd][1] # Decoding from byte to string and getting short hash

    return tag

   