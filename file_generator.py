import os
import re
import json
from datetime import datetime
from git_scrapper import Commit

class File():
    """ Class representing a changelog File """
    def __init__(self, path: str):
        self.__path: str = path


    def get_where_to_write(self) -> int:
        """ Method that retrieve the position of the last changelog section """
        if os.path.exists(self.__path):
            with open(self.__path, 'r') as file:
                lines: list[str] = file.readlines()
                # Creating regex to find section title
                version_pattern = re.compile("## \[[0-9].[0-9].[0-9]\] - [0-9]+-[0-9]+-[0-9]+")
                if len(lines) > 5:
                    # Checking the lines one by one
                    for l in lines:
                        # If we find a section title, return line index
                        if version_pattern.match(l):
                            return lines.index(l)
                else:
                    return 5


    def read_chlog(self) -> list[str]:
        """ Method that opens and read the chlog file lines """
        if os.path.exists(self.__path):
            with open(self.__path, 'r') as file:
                return file.readlines()


    def write_chlog(self, newlines: list[str]):
        """ Method that writes the new lines in the chlog file"""
        if os.path.exists(self.__path):
            with open(self.__path, 'w') as file:
                file.writelines(newlines)

def write_changes(project_path: str, file: File, commits: list[Commit]):
    """ Method that write the given commits by adding a new section to the changelog file """
    # Getting released version
    os.chdir(project_path)
    with open("chlog.json") as chlog_file:
        chlog = json.load(chlog_file)
        version: str = chlog["version"]
    # Creating title (version + date)
    section: str = ["## [" + version[1:] + "] - " + datetime.today().isoformat().split('T')[0] + "\n"]
    # Sorting commits by types
    added = []
    changed = []
    removed = []
    security = []
    doc = []
    for commit in commits:
        if commit.get_type() == "feat":
            added.append(commit.chlog_format())
        elif commit.get_type() == "fix":
            changed.append(commit.chlog_format())
        elif commit.get_type() == "removed":
            removed.append(commit.chlog_format())
        elif commit.get_type() == "security":
            security.append(commit.chlog_format())
        elif commit.get_type() == "doc":
            doc.append(commit.chlog_format())
    # Creating sections (title + unnumbered list)
    if added:
        section.append("### Added\n")
        for e in added:
            section.append("- " + e + "\n")
        section.append("\n")
    if changed:
        section.append("### Changed\n")
        for e in changed:
            section.append("- " + e + "\n")
        section.append("\n")
    if removed:
        section.append("### Removed\n")
        for e in removed:
            section.append("- " + e + "\n")
        section.append("\n")
    if security:
        section.append("### Security\n")
        for e in security:
            section.append("- " + e + "\n")
        section.append("\n")
    if doc:
        section.append("### Doc\n")
        for e in doc:
            section.append("- " + e + "\n")
        section.append("\n")
    # Finally writting everything to file
    lines = file.read_chlog()
    insert_index = file.get_where_to_write()
    lines[insert_index:insert_index] = section
    file.write_chlog(lines)

