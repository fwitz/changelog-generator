#!/usr/bin/python3
import os
import sys
import shutil
import subprocess
import argparse
import json
import string
from datetime import datetime, timedelta

from git_scrapper import *
from file_generator import *
 
def generate_changelog(project_path: str):
    """
    Method used to generate a changelog based on git commit
    """
    check_update() # Checking if an update is available
    check_chlog(project_path=project_path) # Checking if the config file is present on the project directory
    print("Beginning changelog generation... 🛠")
    # Checking formatting of every commit before generation
    correctly_formatted = check_formating(project_path=project_path, before_gen=True)
    # If all commit are correctly formatted
    if correctly_formatted:
        os.chdir(project_path) # Going to the project directory
        # Get the last version in chlog.json to get the appropriate commit
        with open("chlog.json", "r") as chlog_file:
            chlog = json.load(chlog_file)
            version = chlog["last"]
        commits = get_formatted_commits(version)
        # Check if changelog file exist
        if not os.path.exists("CHANGELOG.md"):
            # Creating it and ask for a short description to insert at the begining of the file
            print("No CHANGELOG.md file found, initializing it... 🛠")
            with open("CHANGELOG.md", 'w') as md_file:
                to_write: list[str] = ["# Changelog\n", "\n", "All notable changes to this project will be documented in this file\n", "\n"]
                md_file.writelines(to_write)
        # Instanciate File object
        changelog = File(os.path.abspath("CHANGELOG.md"))
        # Generate version section
        print("Writing changes... 🛠")
        write_changes(project_path, changelog, commits)
        print("Changelog now have a new section ! ✨")
    else:
        print("❌ Some commits don't respect formatting rule, correct them and retry.")


def versioning(args, project_path: str):
    """
    Method to apply a specified versionning tag to git commit history
    """
    check_update() # Checking if an update is available
    check_chlog(project_path=project_path) # Checking if the version file is present on the project directory
    print("Versionning project... 🛠")
    os.chdir(project_path)
    with open('chlog.json', 'r') as version_file:
        chlog = json.load(version_file)
        version = chlog['version'][1:].split('.')
    version = [int(v) for v in version]
    # Depending on the argument, updating the correct part of the version
    if args == 'n':
        version[0] += 1
        version[1] = 0
        version[2] = 0
    elif args == 'u':
        version[1] += 1
        version[2] = 0
    elif args == 'f':
        version[2] += 1
    # Rebuilding version string and write it back to config file. Also adding git tag
    chlog['last'] = chlog['version']
    chlog['version'] = 'v' + str(version[0]) + '.' + str(version[1]) + '.' + str(version[2])
    with open('chlog.json', 'w+') as version_file:
        version_file.write(json.dumps(chlog))
    add_git_tag(chlog['version'], None)
    print("Version updated ! ✨")


def check_chlog(project_path: str):
    """
    Method to check if the chlog.json is on the project directory, if not create it
    """
    os.chdir(project_path)
    # Checking if chlog.json doesn't exist. If it's the case checking .gitignore, create chlog.json and wirte in it
    if not os.path.exists(os.path.join(os.getcwd(), 'chlog.json')):
        # Checking if .gitignore doesn't exist. If it's the case, creating it
        if not os.path.exists(os.path.join(os.getcwd(), '.gitignore')):
            os.mknod(os.path.join(os.getcwd(), '.gitignore'))
        # Adding chlog.json file to .gitignore
        with open(os.path.join(os.getcwd(), '.gitignore'), 'a') as ignore_file:
            ignore_file.write("chlog.json")
        # Init chlog.json file
        os.mknod(os.path.join(os.getcwd(), 'chlog.json'))
        with open(os.path.join(os.getcwd(), 'chlog.json'), 'w') as chlog_file:
            chlog_file.write(json.dumps({"version":"v0.0.0", "last": "v0.0.0"}))
    

def check_update():
    """
    Method to check if their is any update available
    """
    # Moving to the instal directory
    path = os.path.join(os.path.expanduser('~'), '.bin/changelog-generator')
    os.chdir(path)
    # Opening package file and get the last datetime when the tool check for updates
    with open('package.json', 'r') as package_file:
        package = json.load(package_file)
        last_check = datetime.strptime(package['last-update-check'], '%Y-%m-%d %H:%M')
        now = datetime.strptime(datetime.now().strftime('%Y-%m-%d %H:%M'), '%Y-%m-%d %H:%M')
    # If the last check was one day or more in the past
    if last_check + timedelta(days=1) >= now:
        # Checking if the tool repo is cloned, if not we clone it
        if not os.path.exists('repo/'):
            subprocess.check_output(
                ['git', 'clone', 'git@framagit.org:fwitz/changelog-generator.git'], stderr=subprocess.STDOUT
            )
            os.rename('changelog-generator', 'repo') # Renaming the repo to avoid ambiguity
        # Then, we move in the repo, git pull and compare the tool versions's. If their is a difference,
        # the tool alert the user that an update is available
        os.chdir('repo/')
        subprocess.check_output(
            ['git', 'pull'], stderr=subprocess.STDOUT
        )
        # Getting versions
        with open('package.json', 'r') as package_file_repo:
            package_repo = json.load(package_file_repo)
            version_repo = package_repo['chlog-version']
            version = package['chlog-version']
        # Checking versions
        if version_repo > version:
            print('\033[93mAn update is available ! Run chlog --update to update the tool.\033[0m')
        # Finally, we save the actual date as the last update check in the package.json file
        os.chdir('..')
        with open('package.json', 'w') as package_file:
            package['last-update-check'] = now.strftime('%Y-%m-%d %H:%M')
            package_file.write(json.dumps(package))


def update():
    """ Method used to update the tool """
    print("Updating chlog tool... 🛠")
    # Moving to the instal directory
    path = os.path.join(os.path.expanduser('~'), '.bin/changelog-generator')
    os.chdir(path)
    # Checking if the tool repo is cloned, if not we clone it
    if not os.path.exists('repo/'):
        subprocess.check_output(
            ['git', 'clone', 'git@framagit.org:fwitz/changelog-generator.git'], stderr=subprocess.STDOUT
        )
        os.rename('changelog-generator', 'repo') # Renaming the repo to avoid ambiguity
    os.chdir('repo/')
    subprocess.check_output(
        ['git', 'pull'], stderr=subprocess.STDOUT
    )
    # Updating tool by copying python files
    os.chdir(path)
    for filename in os.listdir('repo/'):
        ext: str = os.path.splitext(filename)[1][1:]
        if ext == "py":
            if filename == "chlog.py":
                os.remove("chlog")
                shutil.copy("repo/chlog.py", path)
                os.rename("chlog.py", "chlog")
                os.chmod("chlog", 0o711)
            else:
                shutil.copy(os.path.join("repo/", filename), path)
        elif ext == "json":
            if filename == "package.json":
                # Update version in the installed tool package.json
                with open("repo/package.json", 'r') as new_package:
                    data = json.load(new_package)
                with open("package.json", 'r') as package:
                    package_json = json.load(package)
                package_json["chlog-version"] = data["chlog-version"]
                with open("package.json", "w") as package:
                    package.write(json.dumps(package_json))
            elif filename == "config.json":
                # First opening both actual and new config file
                with open("config.json", 'r') as config_file:
                    config = json.load(config_file)
                with open("repo/config.json", 'r') as new_config_file:
                    new_config = json.load(new_config_file)
                # Then copying the new lines into the config file if their is any
                for k, v in new_config.items():
                    if not k in config:
                        config[k] = v
    print("Tool updated ! ✨")


def check_case(case_type: str, to_check: str) -> bool:
    """ Method that check if the given string has the correct case """
    if case_type == "lowercase":
        return to_check.islower()
    elif case_type == "uppercase":
        return to_check.isupper()
    elif case_type == "capitalized":
        return to_check[0].isupper()


def check_formating(project_path: str, before_gen: bool):
    """ Method that check if the commits follow the formating rules set in the config """
    print("Checking commit formatting since last version... 🛠\n")
    check_update()
    check_chlog(project_path=project_path)
    # Retrieving formatting rules in config
    path = os.path.join(os.path.join(os.path.expanduser("~"), ".bin"), "changelog-generator")
    with open(os.path.join(path, 'config.json'), 'r') as file:
        config = json.load(file)
        commit_type: list[str] = [e[0] for e in config['commit-type']]
        ignored_commit_type: list[str] = [e[0] for e in config['ignored-commit-type']]
        type_case: str = config['commit-type-case']
        subject_case: str = config['commit-subject-case']
        has_punctuation: bool = config['commit-subject-ponctuation']
        subject_length: int = config['commit-subject-length']
    # Getting all commits since last changeloged version
    os.chdir(project_path)
    with open('chlog.json', 'r') as file:
        chlog = json.load(file)
        if before_gen:
            version: str = chlog['last']
        else:
            version: str = chlog['version']
    commits: list[list[str]] = get_raw_commits(version)

    format_pattern = re.compile("[a-z]+:(\s|\S)(?:[a-z]+(\s|\S))+")
    # Checking, for each commit, if it respects the set of rules
    for commit in commits:
        # First, we check if the global formatting
        if format_pattern.match(commit[1]):
            ctype, message = commit[1].split(":")
            if ctype in commit_type or ctype in ignored_commit_type:
                if check_case(type_case, ctype):
                    if len(message) <= subject_length:
                        if check_case(subject_case, message):
                            if not has_punctuation:
                                punctuation = set(string.punctuation)
                                if not any(char in punctuation for char in message):
                                    print("✅ " + commit[1])
                                else:
                                    print("❌ " + commit[1] + " ➡ The commit message must not contain special characters")
                                    if before_gen:
                                        return False
                            else:
                                print("✅ " + commit[1])
                        else:
                            print("❌ " + commit[1] + " ➡ The commit message has to be " + subject_case)
                            if before_gen:
                                return False
                    else:
                        print("❌ " + commit[1] + " ➡ The commit message must be a maximum of " + str(subject_length) + " characters")
                        if before_gen:
                            return False
                else:
                    print("❌ " + commit[1] + " ➡ The commit type has to be " + type_case)
                    if before_gen:
                        return False
            else:
                print("❌ " + commit[1] + " ➡ The commit type is unknown, add it to the configuration")
                if before_gen:
                    return False
        else:
            print("❌ " + commit[1] + " ➡ This commit doesn't follow the intended format type: message")
            if before_gen:
                return False
    print("\nCheck finished ! ✨")   
    if before_gen:
        return True 
    # Explain to user how to edit the commits with git
    if not before_gen:
        print("ℹ You can edit your commit messages using the interactive version of the git rebase command !")


def rules():
    """ Method to edit or show the formating rules """
    print("Welcome to the changelog rules editor ! 🛠\n")
    check_update()
    # Retrieving formatting rules in config
    path = os.path.join(os.path.join(os.path.expanduser("~"), ".bin"), "changelog-generator")
    with open(os.path.join(path, 'config.json'), 'r') as file:
        config = json.load(file)
        commit_type: list[str] = config['commit-type']
        ignored_commit_type: list[str] = config['ignored-commit-type']
        type_case: str = config['commit-type-case']
        subject_case: str = config['commit-subject-case']
        has_punctuation: bool = config['commit-subject-ponctuation']
        subject_length: int = config['commit-subject-length']
    print("ℹ Here are the actual rules of your changelog tool.\n")
    print("1. Standard commit types: ")
    for e in commit_type:
        print("    - " + e[0] + ": " + e[1])
    print("2. Ignored commit types: ")
    for e in ignored_commit_type:
        print("    - " + e[0] + ": " + e[1])
    print("3. Commit type case: ", type_case)
    print("4. Commit subject case: ", subject_case)
    print("5. Commit subject is allowed to contain punctuation: ", str(has_punctuation))
    print("6. Commit subject length: ", str(subject_length))
    # Asking user if he wants to edit those or simply leave
    choice = ""
    while choice == "" and choice != 'q' and choice != 'e':
        choice = input("Press q to quit or e to edit: ")
    # Going through config editing
    if choice == 'e':
        continue_edit = True
        print("\nLet's edit the rules ! 🛠")
        while continue_edit:
            option = ""
            while not option in ["1","2","3","4","5","6"] and option == "" and option != "q":
                option = input("Please choose a specific rule to edit (1-6, q to quit): ")
            if option == "1":
                # Standard commit type edition
                action = ""
                while action == "" and not action in ["1", "2", "3"] and action != "q":
                    print("1. Add a new standard commit type")
                    print("2. Remove a standard commit type")
                    action = input("What do you whant to do ? (q to quit) ")
                if action == "1":
                    name = ""
                    while name == "":
                        name = input("Enter standard commit type name: ")
                    description = ""
                    while description == "":
                        description = input("Enter standard commit type short description: ")
                    validation = ""
                    while validation == "" and not validation in ["y", "n"]:
                        validation = input("Do you want to add this new standard commit type ? (y/n) ")
                    if validation == "y":
                        config['commit-type'].append([name, description])
                elif action == "2":
                    print("\nStandard commit type:")
                    index = 1
                    for e in commit_type:
                        print("    " + str(index) + ". " + e[0] + ": " + e[1])
                        index +=1
                    number = ""
                    while number == "" and not number in [str(i) for i in range(1, len(config['commit-type'])+1)] and number != "q":
                        number = input("\nWhich standard commit type do you want to remove ? (q to quit) ")
                    if number != "q":
                        validation = ""
                        while validation == "" and not validation in ["y", "n"]:
                            validation = input("Do you really want to remove this standard commit type ? (y/n) ")
                        if validation == "y":
                            config["commit-type"].remove(config["commit-type"][int(number)-1])
            elif option == "2":
                # Ignored commit type edition
                action = ""
                while action == "" and not action in ["1", "2", "3"] and action != "q":
                    print("1. Add a new ignored commit type")
                    print("2. Remove a ignored commit type")
                    action = input("What do you whant to do ? (q to quit) ")
                if action == "1":
                    print("\n")
                    name = ""
                    while name == "":
                        name = input("\Enter ignored commit type name: ")
                    description = ""
                    while description == "":
                        description = input("Enter ignored commit type short description: ")
                    validation = ""
                    while validation == "" and not validation in ["y", "n"]:
                        validation = input("Do you want to add this new ignored commit type ? (y/n) ")
                    if validation == "y":
                        config['ignored-commit-type'].append([name, description])
                elif action == "2":
                    print("\nIgnored commit type:")
                    index = 1
                    for e in ignored_commit_type:
                        print("    " + str(index) + ". " + e[0] + ": " + e[1])
                        index +=1
                    number = ""
                    while number == "" and not number in [str(i) for i in range(1, len(config['ignored-commit-type'])+1)] and number != "q":
                        number = input("\nWhich ignored commit type do you want to remove ? (q to quit) ")
                    if number != "q":
                        validation = ""
                        while validation == "" and not validation in ["y", "n"]:
                            validation = input("Do you really want to remove this ignored commit type ? (y/n) ")
                        if validation == "y":
                            config["ignored-commit-type"].remove(config["ignored-commit-type"][int(number)-1])
            elif option == "3":
                # Commit type case edition
                print("\nPossible case:")
                print("    1. lowercase")
                print("    2. uppercase")
                print("    3. capitalized")
                new_type_case = ""
                while new_type_case == "" and not new_type_case in ["1", "2", "3"] and new_type_case != "q":
                    new_type_case = input("New type case (1-3, q to quit): ")
                if new_type_case == "1":
                    config['commit-type-case'] = "lowercase"
                elif new_type_case == "2":
                    config['commit-type-case'] = "uppercase"
                elif new_type_case == "3":
                    config['commit-type-case'] = "capitalized"
            elif option == "4":
                # Subject message case edition 
                print("\nPossible case:")
                print("    1. lowercase")
                print("    2. uppercase")
                print("    3. capitalized")
                new_subject_case = ""
                while new_subject_case == "" and not new_subject_case in ["1", "2", "3"] and new_subject_case != "q":
                    new_subject_case = input("New type case (1-3, q to quit): ")
                if new_subject_case == "1":
                    config['commit-subject-case'] = "lowercase"
                elif new_subject_case == "2":
                    config['commit-subject-case'] = "uppercase"
                elif new_subject_case == "3":
                    config['commit-subject-case'] = "capitalized"
            elif option == "5":
                # Punctuation rule edition 
                new_punct = ""
                while new_punct == "" and not new_punct in ["y", "n"] and new_punct != "q":
                    new_punct = input("Allowing punctuation in commit subject (y/n, q to quit): ")
                if new_punct == "y":
                    config['commit-subject-ponctuation'] = True
                elif new_punct == "n":
                    config['commit-subject-ponctuation'] = False
            elif option == "6":
                # Subject length edition
                new_length = ""
                while new_length == "" and not new_length.isnumeric() and new_length != "q":
                    new_length = input("New subject length (q to quit): ")
                if new_length != "q":
                    config['commit-subject-length'] = int(new_length)
            # Asking user if he wants to edit anything else
            edition = ""
            while edition == "" and not edition in ["y", "n"]:
                edition = input("Do you want to edit another rule ? (y/n) ")
            if edition == "y":
                continue_edit = True
            elif edition == "n":
                continue_edit = False
        # Write change to the config file
        with open(os.path.join(path, 'config.json'), 'w') as file:
            file.write(json.dumps(config))
        print("Rules succesfully edited ! ✨")    
    elif choice == 'q':
        print("Good bye ✨")  


def config(argv):
    """ Method defining the cli tool options and actions """
    parser = argparse.ArgumentParser(add_help=True, prog="chlog")
    parser.add_argument(
        "-g",
        "--generate",
        action="store_true",
        help="generate a changelog"
    )
    parser.add_argument(
        "-v",
        "--version",
        nargs=1,
        choices=['n','u','f'],
        help="update the software version"
    )
    parser.add_argument(
        "-c",
        "--check",
        action="store_true",
        help="check commit formatting"
    )
    parser.add_argument(
        "-u",
        "--update",
        action="store_true",
        help="update the tool"
    )
    parser.add_argument(
        "-r",
        "--rules",
        action="store_true",
        help="edit or show commit formatting rules"        
    )
    parser.add_argument(
        "-p",
        "--path",
        nargs=1,
        help="specify project path"
    )
    args = parser.parse_args()
    if args.generate:
        if(args.path[0]):
            generate_changelog(project_path=args.path[0])
        else:
            print("You need to provide a path to the project with the -p option !")
    elif args.version:
        if(args.path[0]):
            versioning(args=args.version[0], project_path=args.path[0])
        else:
            print("You need to provide a path to the project with the -p option !")
    elif args.check:
        if(args.path[0]):
            check_formating(project_path=args.path[0], before_gen=False)
        else:
            print("You need to provide a path to the project with the -p option !")
    elif args.update:
        update()
    elif args.rules:
        rules()


if __name__ == "__main__":
    if sys.argv[1:] != []:
        config(sys.argv[1:])
    else:
        print(
            "Arguments incorrect. Type chlog --help for more informations"
        )