#!/usr/bin/python3
import os
import sys
import json
from datetime import datetime
import shutil

# Declaring different path for the next steps
home = os.path.expanduser("~")
target = os.path.join(home, ".bin")
final_dir = os.path.join(target, "changelog-generator")
actual = os.getcwd()

useless_files = ["README.md", ".git", ".gitignore"]

print("Welcome to Chlog CLI ! Begining instalation... 🛠")
print("Removing useless files... 🛠")

# Removing useless files in the user context
for file in useless_files:
    if os.path.isdir(file):
        shutil.rmtree(file, ignore_errors=True)
    else:
        os.remove(file)

# Checking if local bin directory exist, if not creating it
if not os.path.exists(target):
    print("~/.bin directory not found. Creating it... 🛠")
    os.mkdir(target)

# Copying all file in the local binary directory
if not os.path.exists(final_dir):
    print("Copying files in the correct directory... 🛠")
    os.mkdir(final_dir)
    i = 0
    for filename in os.listdir(actual):
        # Making some change to the command file to use it as a custome command
        if filename == "chlog.py":
            os.rename(filename, "chlog")
            os.chmod("chlog", 0o711)
            shutil.copy("chlog", final_dir)
        else:
            shutil.copy(filename, final_dir)

        i += 1
        print(str(i) + "/" + str(len(os.listdir(actual))) + " files copied... 🛠")
    
    # Putting the install date as the last update check in the package file
    with open(os.path.join(final_dir, 'package.json'), 'r+') as package_file:
        package = json.load(package_file)
        now = datetime.now().strftime("%Y-%m-%d %H:%M")
    
    package['last-update-check'] = now
    with open(os.path.join(final_dir, 'package.json'), 'w') as package_file:
        package_file.write(json.dumps(package))

# Adding local bin directory to PATH and make it permanent in shell config files
shell = os.getenv("SHELL").split("/")[2] # Getting user shell
print("Configuring shell... 🛠")
export = 'export PATH=$PATH":$HOME/.bin/changelog-generator/"'
export_fish = "set -xg PATH $HOME/.bin/changelog-generator/ $PATH"
# Checking if user uses fish, because it's configured and work differentlty than usual shell solution like zsh or bash
if shell != "fish":
    with open(home + "/." + shell + "rc", "a+") as config:
        lines = config.readlines()
        if not export in lines:
            config.write(export)
else:
    with open(home + "/.fish/config.fish", "a+") as config:
        lines = config.readlines()
        if not export in lines:
            config.write(export)

# Deleting cloned folder, there is no need of it
os.chdir(home)
shutil.rmtree(actual, ignore_errors=True)
print("Chlog CLI successfully installed ! ✨")
sys.exit(0)
